package com.ezanetta.pokemosby.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ezanetta.pokemosby.R;
import com.ezanetta.pokemosby.api.PokemonEntries;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ezanetta on 9/09/16.
 */
public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonVH>{

    private List<PokemonEntries.PokemonEntry> pokemons;

    public PokemonAdapter(List<PokemonEntries.PokemonEntry> pokemons) {
        this.pokemons = pokemons;
    }

    @Override
    public PokemonVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pokemon_item, parent, false);

        return new PokemonVH(view);
    }

    @Override
    public void onBindViewHolder(PokemonVH holder, int position) {
        holder.pokemonTitle.setText(pokemons.get(position).getPokemon().getName());
    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    public static class PokemonVH extends RecyclerView.ViewHolder{

        @BindView(R.id.pokemonTitle) TextView pokemonTitle;

        public PokemonVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
