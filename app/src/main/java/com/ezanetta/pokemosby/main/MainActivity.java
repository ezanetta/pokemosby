package com.ezanetta.pokemosby.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.ezanetta.pokemosby.R;
import com.ezanetta.pokemosby.api.PokemonEntries;
import com.ezanetta.pokemosby.api.RestClient;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpActivity<MainView, MainPresenter> implements MainView {

    @BindView(R.id.pokemonRV) RecyclerView pokemonRV;
    @BindView(R.id.pokeProgress)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter(new FindPokemonsInteractorImpl(new RestClient()));
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setPokemons(List<PokemonEntries.PokemonEntry> pokemons) {
        pokemonRV.setLayoutManager(new LinearLayoutManager(this));
        pokemonRV.setAdapter(new PokemonAdapter(pokemons));
    }
}
