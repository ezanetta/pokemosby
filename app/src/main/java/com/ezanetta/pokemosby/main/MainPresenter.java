package com.ezanetta.pokemosby.main;

import android.util.Log;

import com.ezanetta.pokemosby.api.PokemonEntries;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by ezanetta on 9/09/16.
 */
public class MainPresenter implements MvpPresenter<MainView>,
        FindPokemonsInteractor.OnFinishedListener {

    private static final String TAG = MainPresenter.class.getSimpleName();
    private FindPokemonsInteractor findPokemonsInteractor;
    private MainView view;

    public MainPresenter(FindPokemonsInteractor findPokemonsInteractor) {
        this.findPokemonsInteractor = findPokemonsInteractor;
    }

    @Override
    public void attachView(MainView view) {
        this.view = view;
        findPokemonsInteractor.findPokemons(this);
    }

    @Override
    public void detachView(boolean retainInstance) {

    }

    @Override
    public void onFinished(List<PokemonEntries.PokemonEntry> pokemons) {
        Log.d(TAG, "onFinished");
        view.setPokemons(pokemons);
        view.hideProgress();
    }
}
