package com.ezanetta.pokemosby.main;

import com.ezanetta.pokemosby.api.PokemonEntries;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

/**
 * Created by ezanetta on 9/09/16.
 */
public interface MainView extends MvpView {

    void hideProgress();

    void setPokemons(List<PokemonEntries.PokemonEntry> pokemons);
}
